# Phabeola #

Phabeola image sharing service.

### Install Requirements ###

+ Apache httpd >= 2.2.15
+ PHP >= 5.3.3
+ php-mcrypt
+ MySQL >= 5.1
+ libmcrypt

### Known Issues ###

A trailing / must be applied on requests to the API for anything other than get. Otherwise the API will default to a get request... for some reason.