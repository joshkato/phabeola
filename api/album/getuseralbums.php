<?php

require('../data/AlbumRepository.class.php');

header('Content-type: application/json');

$response = array(
	'code' => -1,
	'message' => ''
);

$albumRepo = new AlbumRepository();

$userId = (isset($_POST['userid']) ? $_POST['userid'] : 0);

if($userId == 0)
{
	$response['code'] = -1;
	$response['message'] = 'A User ID must be set.';
	echo json_encode($response);
	return;
}

$albums = $albumRepo->GetAlbumsByUser($userId);
$response['code'] = 0;
$response['message'] = 'Found ' . sizeof($albums) . ' albums.';
$response['dataType'] = 'album';
$response['data'] = $albums;
echo json_encode($response);
return;

?>