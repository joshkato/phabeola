<?php

header('Content-type: application/json');

$reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
$jsonResponse = '';

if($reqMethod == 'get')
{
	//$jsonResponse = UserGet();	
}
else if($reqMethod == 'post')
{
	//$jsonResponse = UserPost();
}

else if($reqMethod == 'put')
{
	//$jsonResponse = UserPut();
}

else if($reqMethod == 'delete')
{
	//$jsonResponse = UserDelete();
}
else
{
	$response = array(
		'code' => -1,
		'message' => 'Unsupported HTTP method.'
	);
	$jsonResponse = json_encode($response);
}

// Output the response
echo $jsonResponse;

?>