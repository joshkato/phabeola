<?php

header('Content-type: application/json');

require_once $_SERVER['DOCUMENT_ROOT'] .'/phabeola/api/lib/bcrypt.php';

$response = array('code' => -1, 'message' => 'No text to encrypt');

if(!isset($_POST['text']))
{
	echo json_encode($response);
	return;
}

$response['hash'] = password_hash($_POST['text'], PASSWORD_BCRYPT);
$response['code'] = 0;
$response['message'] = '';
echo json_encode($response);
return;

?>