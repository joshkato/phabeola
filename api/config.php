<?php

// Configuration definitions

$CONFIG_OPTIONS = array(
    // User API Settings
	'loginurl' => 'api/user/login',
	'logouturl' => 'api/user/logout',
    'registerurl' => '/phabeola/api/user/',
    'pwupdateurl' => '/phabeola/api/user/pwupdate.php'
);

// End configuration definitions

header('Content-type: application/json');

$response = array();

$reqMethod = strtolower($_SERVER['REQUEST_METHOD']);

// Check to make sure that the request was made using POST
if($reqMethod != 'post')
{
	header('HTTP/1.1 400 Bad Request');
	$response['error'] = 'Unsupported HTTP method.';
	echo json_encode($response);
	return;
}

// Attempt to get the configuration option that was requested.
$configOpt = isset($_POST['option']) ? strtolower($_POST['option']) : null;

// If the configuration option is not set or its set to "nothing"
// return an error.
if($configOpt == null || $configOpt == '')
{
	header('HTTP/1.1 400 Bad Request');
	$response['error'] = 'No configuration option name was provided.';
	echo json_encode($response);
	return;
}

// Get the value for the configuration option
$optValue = $CONFIG_OPTIONS[$configOpt];

// If the value does not exist, return an error.
if($optValue == null)
{
	header('HTTP/1.1 400 Bad Request');
	$response['error'] = '\'' . $configOpt . '\' is not a configuration option.';
	echo json_encode($response);
	return;
}

$response['value'] = $optValue;
echo json_encode($response);
return;


?>