<?php

include('DbProvider.class.php');

class AlbumRepository
{
	private $_dbProvider;
	
	function __construct()
	{
		$this->_dbProvider = new DbProvider();
	}
	
	public function GetAlbumsByUser($userId)
	{
		$query = 'SELECT `ID`, `Title`, DATE(`CreationDate`) `Created`, `Description`, `IsPrivate` FROM `phabeola`.`album` WHERE `UserID` = :uid';
		
		$conn = $this->_dbProvider->GetConnection();
		
		$statement = $conn->prepare($query);
		
		$statement->execute(array('uid' => $userId));
		
		$albums = array();
		foreach($statement as $row)
		{
			array_push($albums, array(
				'ID' => $row['ID'],
				'Title' => $row['Title'],
				'CreationDate' => $row['Created'],
				'Description' => $row['Description'],
				'IsPrivate' => ($row['IsPrivate'] >= 1 ? true : false)
			));
		}
		
		return $albums;
	}
}

?>