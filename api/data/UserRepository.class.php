<?php

include('DbProvider.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/phabeola/api/lib/bcrypt.php');

// User IDs CANNOT leave this class without being
// encoded first.

require_once('../lib/HashUtil.class.php');

class UserRepository
{
	private $_dbProvider;
    private $_hashUtil;
		
	function __construct()
	{
		$this->_dbProvider = new DbProvider();
        $this->_hashUtil = new HashUtil('user');
	}
	
	public function LoginUser($userName, $userPass)
	{	
		$userQuery = 'SELECT `ID`, `LoginName`, `Password` FROM `phabeola`.`user` WHERE `LoginName` = :name';
		
		$conn = $this->_dbProvider->GetConnection();
		
		$statement = $conn->prepare($userQuery);
		
		$statement->execute(array(
			'name' => $userName
		));
		
		$row = $statement->fetch();
		
		if(password_verify($userPass, $row['Password']))
		{
            $encodedId = $this->_hashUtil->Encode($row['ID']);
			return $encodedId;
		}
		
		return null;
	}

    public function AuthenticateUser($userId, $password)
    {
        $authQuery = 'SELECT `Password` FROM `phabeola`.`user` WHERE `ID` = :id';

        $decodedUserId = $this->_hashUtil->Decode($userId);

        $conn = $this->_dbProvider->GetConnection();
        $statement = $conn->prepare($authQuery);
        $statement->execute(array(
           'id' => $decodedUserId
        ));

        $row = $statement->fetch();

        if(password_verify($password, $row['Password']))
        {
            return true;
        }

        return false;
    }
	
	public function CreateUser($userName, $userPassword, $userEmail)
	{
		$insQuery = 'INSERT INTO `phabeola`.`user` (`LoginName`, `Password`, `Email`) VALUES (:name, :pass, :email)';
		
		$conn = $this->_dbProvider->GetConnection();
		
		$statement = $conn->prepare($insQuery);
		
		$statement->execute(array(
			'name' => strtolower($userName),
			'pass' => password_hash($userPassword, PASSWORD_BCRYPT),
			'email' => $userEmail
		));
		
		return $conn->lastInsertId();
	}
	
	public function GetUser($userId)
	{
        $getQuery = 'SELECT `ID`, `LoginName`, `Password`, `Email`, `FirstName`, `LastName`, `DOB`, `IsAdmin` FROM `phabeola`.`user` WHERE `ID` = :id';
		
		$conn = $this->_dbProvider->GetConnection();
		
		$statement = $conn->prepare($getQuery);

        $decodedId = $this->_hashUtil->Decode($userId, true);

		$statement->execute(array(
			'id' => $decodedId
		));
		
		$row = $statement->fetch();
		
		if($row != null)
		{
            $encodedId = $this->_hashUtil->Encode($row['ID']);

			return array(
				'ID' => $encodedId,
				'LoginName' => $row['LoginName'],
                'Password' => $row['Password'],
				'Email' => $row['Email'],
				'FirstName' => $row['FirstName'],
				'LastName' => $row['LastName'],
				'DOB' => $row['DOB'],
                'IsAdmin' => $row['IsAdmin'] > 0 ? true : false
			);
		}
		
		return null;
	}

    public function UpdateUser($userData)
    {
        $updateQuery = 'UPDATE `phabeola`.`user` SET `Email` = :email, `FirstName` = :firstName, `LastName` = :lastName, `DOB` :dob WHERE `ID` = :id';

        $conn = $this->_dbProvider->GetConnection();

        $statement = $conn->prepare($updateQuery);

        // Only email, first/last name, and DOB should be allowed to be updated
        // through this method
        // TODO Maybe add "admin" update to override password/username?
        $statement->execute(array(
            'email' => $userData['Email'],
            'firstName' => $userData['FirstName'],
            'lastName' => $userData['LastName'],
            'DOB' => $userData['DOB']
        ));
    }

    public function UpdatePassword($userId, $password)
    {
        $updateQuery = 'UPDATE `phabeola`.`user` SET `Password` = :password WHERE `ID` = :id';

        $conn = $this->_dbProvider->GetConnection();

        $statement = $conn->prepare($updateQuery);

        $decodedUserId = $this->_hashUtil->Decode($userId);

        $statement->execute(array(
            'password' => password_hash($password, PASSWORD_BCRYPT),
            'id' => $decodedUserId
        ));
    }

    public function UserExists($userName)
    {
        $query = 'SELECT COUNT(*) UserCount FROM `phabeola`.`user` WHERE LoginName = :name';

        $conn = $this->_dbProvider->GetConnection();

        $statement = $conn->prepare($query);

        $statement->execute(array(
            'name' => $userName
        ));

        $row =$statement->fetch();

        if($row != null)
        {
            if($row['UserCount'] > 0)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }
}

?>