<?php

include('lib/HashUtil.class.php');

$HASH_MODE_ENCRYPT = 'encrypt';
$HASH_MODE_DECRYPT = 'decrypt';

session_start();

header('Content-type: application/json');

$response = array(
    'code' => 0,
    'message' => ''
);

if(!isset($_SESSION['userData']))
    return;

$userData = $_SESSION['userData'];

if(!$userData['IsAdmin'])
{
    header('HTTP/1.1 401 Unauthorized');
    $response['code'] = -1;
    $response['message'] = 'User cannot access this function.';
    echo json_encode($response);
    return;
}

$hashType = isset($_POST['type']) ? strtolower($_POST['type']) : '';
$hashMode = isset($_POST['mode']) ? strtolower($_POST['mode']) : '';
$value = isset($_POST['value']) ? $_POST['value'] : '';

if($hashType == '' || $hashMode == '' || $value == '')
{
    header('HTTP/1.1 400 Bad Request');
    $response['code'] = -2;
    $response['message'] = 'Missing field.';
    echo json_encode($response);
    return;
}

if($hashMode != $HASH_MODE_ENCRYPT && $hashMode != $HASH_MODE_DECRYPT)
{
    header('HTTP/1.1 400 Bad Request');
    $response['code'] = -3;
    $response['message'] = $hashMode . ' is not a valid hashing mode.';
    echo json_encode($response);
    return;
}

$hashUtil = null;
try
{
    $hashUtil = new HashUtil($hashType);
}
catch(Exception $ex)
{
    header('HTTP/1.1 400 Bad Request');
    $response['code'] = -4;
    $response['message'] = $hashType . ' is not a valid hash type.';
    echo json_encode($response);
    return;
}

if($hashMode == $HASH_MODE_ENCRYPT)
{
    $response['code'] = 0;
    $response['message'] = '';
    $response['value'] = $hashUtil->Encode((int)$value);
    echo json_encode($response);
    return;
}

if($hashMode == $HASH_MODE_DECRYPT)
{
    $response['code'] = 0;
    $response['message'] = '';
    $response['value'] = $hashUtil->Decode($value, true);
    echo json_encode($response);
    return;
}

header('HTTP/1.1 500 Internal Server Error');
$response['code'] = -5;
$response['message'] = 'Unspecified error';
$response['data'] = array('type' => $hashType, 'mode' => $hashMode, 'value' => 'value');
echo json_encode($response);
return;