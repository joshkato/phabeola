<?php

require_once('lib/Hashids.php');

header('Content-type: application/json');

$response = array(
	'hash' => ''
);

//$id = (isset($_POST['id']) ? $_POST['id'] : 0);
//$salt = (isset($_POST['salt']) ? $_POST['salt'] : '');
//
//if($id == 0 || $salt == '')
//{
//	echo json_encode(array('code' => 1, 'message' => 'An ID and a salt are required.'));
//	return;
//}

$hashIds = new Hashids\Hashids('phabeola');
$hash = $hashIds->encrypt(1, '12345', 6798);
$rawValues = $hashIds->decrypt($hash);

$response['hash'] = $hash;
$response['raw'] = $rawValues[0];
echo json_encode($response);
return;

?>