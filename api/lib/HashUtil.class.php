<?php

include('Hashids.php');

class HashUtil
{
    private $IdHasher;

    private static $IdKeys = array(
        'user' => 'Qs7LoU5XGe9h3sx5vzydr2bdzK3WgEVUSuMkKDP53fKHN5cydGt1Lb282WVVvF32',
        'image' => 'pppKyXNpE9wxqfKEmTdlzGnBKsmqwOYLXdbKRfH83k4Z4WxLvsdRuk1JT4LCAe5c',
        'album' => 'dFkbj8pvn1zMVC2729ri4L925J4z65S7y38319tP9O6s6ZL2lQUz62mcH2E1Z3jK'
    );

    function __construct($hashType)
    {
        $hashKey = self::$IdKeys[$hashType];

        if($hashKey == '' || $hashKey == null)
            throw new Exception('Invalid hash type.');

        $key = hash('whirlpool', $hashKey);

        $this->IdHasher = new \Hashids\Hashids($key, 10);
    }

    public function Encode($id)
    {
        return $this->IdHasher->encrypt($id);
    }

    public function Decode($encryptedId, $firstVal = true)
    {
        $decode = $this->IdHasher->decrypt($encryptedId);

        if($firstVal)
            return $decode[0];
        else
            return $decode;
    }
}