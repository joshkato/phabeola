<?php

/*
Creating a user:
Requires: (x-www-form-urlencoded)
username
password
passwordCheck
email

Updating a user:



*/
session_start();

include('userget.php');
include('usercreate.php');
include('userupdate.php');

header('Content-type: application/json');

$reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
$response = array();

if($reqMethod == 'get')
{
    $userId = (isset($_GET['id']) || $_GET['id'] == '') ? $_GET['id'] : -1;

	$response = UserGet($userId); // defined in userget.php
}
else if($reqMethod == 'post')
{
    if(!isset($_GET['id']))
    {
        $username = (isset($_POST['username']) ? $_POST['username'] : '');
        $password = (isset($_POST['password']) ? $_POST['password'] : '');
        $passwordCheck = (isset($_POST['passwordCheck']) ? $_POST['passwordCheck'] : '');
        $email = (isset($_POST['email']) ? $_POST['email'] : '');

        $response = UserPost($username, $password, $passwordCheck, $email); // defined in usercreate.php
    }
    else
    {

    }
}
else if($reqMethod == 'delete')
{
	//$jsonResponse = UserDelete();
}
else
{
	$response = array(
		'code' => -1,
		'message' => 'Unsupported HTTP method.'
	);
	$response = json_encode($response);
}

// Output the request method along with the response.
$response['method'] = $reqMethod;
echo json_encode($response);

?>