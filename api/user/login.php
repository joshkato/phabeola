<?php

session_start();

require '../data/UserRepository.class.php';

header('Content-type: application/json');

$users = new UserRepository();

$response = array(
	'code' => -1,
	'message' => ''
);

$username = (isset($_POST['username']) ? $_POST['username'] : '');
$password = (isset($_POST['password']) ? $_POST['password'] : '');

if($username == '' || $password == '')
{
	$response['code'] = -1;
	$response['message'] = 'A username or password was not sent.';
	echo json_encode($response);
	return;	
}

$userId = $users->LoginUser($username, $password);

if($userId == null)
{
	$response['code'] = 1;
	$response['message'] = 'Login Failed';
	echo json_encode($response);
	return;	
}

$_SESSION['userData'] = $users->GetUser($userId);

$response['code'] = 0;
$response['message'] = 'Login OK';
$response['UserID'] = $userId;
echo json_encode($response);
return;

?>