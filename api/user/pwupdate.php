<?php

require_once('../lib/HashUtil.class.php');
require_once('../data/UserRepository.class.php');

header('Content-type: application/json');

session_start();

$response = array(
    'code' => 0,
    'message' => ''
);

$hashUtil = new HashUtil('user');
$userRepo = new UserRepository();

$userData = isset($_SESSION['userData']) ? $_SESSION['userData'] : null;
$oldPass = isset($_POST['oldpass']) ? $_POST['oldpass'] : '';
$newPass = isset($_POST['newpass']) ? $_POST['newpass'] : '';
$newPassCheck = isset($_POST['newpasscheck']) ? $_POST['newpasscheck'] : '';

if($userData == null)
{
    header('HTTP/1.1 401 Unauthorized');
    $response['code'] = -1;
    $response['message'] = 'You must be logged in order to use this feature';
    echo json_encode($response);
    return;
}

$response['args'] = array('oldpass' => $oldPass, 'newpass' => $newPass, 'newpasscheck' => $newPassCheck);

if($oldPass == '' || $newPass == '' || $newPassCheck == '')
{
    header('HTTP/1.1 400 Bad Request');
    $response['code'] = -2;
    $response['message'] = 'A required paramater was missing from the request.';
    echo json_encode($response);
    return;
}

if(!$userRepo->AuthenticateUser($userData['ID'], $oldPass))
{
    header('HTTP/1.1 401 Unauthorized');
    $response['code'] = 2;
    $response['message'] = 'You do not have the required permissions to update that password.';
    echo json_encode($response);
    return;
}

if($newPass != $newPassCheck)
{
    header('HTTP/1.1 400 Bad Request');
    $response['code'] = 1;
    $response['message'] = 'Passwords do not mach';
    echo json_encode($response);
    return;
}

$userRepo->UpdatePassword($userData['ID'], $newPass);

$response['code'] = 0;
$response['message'] = 'Password successfully updated.';
echo json_encode($response);