<?php

require_once('../data/UserRepository.class.php');

function UserPost($username = '', $password = '', $passwordCheck = '', $email = '')
{
	$users = new UserRepository();
	
	$response = array(
		'code' => -1,
		'message' => ''
	);

    /*// For Debug
    $response['username'] = $username;
    $response['password'] = $password;
    $response['passwordCheck'] = $passwordCheck;
    $response['email'] = $email;
    */
	
	// Make sure that we have valid input to create a user.
	if($username == '')
	{
		$response['code'] = -1;
		$response['message'] = 'Missing or invalid username.';
		return $response;
	}
	
	if($password == '')
	{
		$response['code'] = -2;
		$response['message'] = 'Missing or invalid password.';
		return $response;
		
	}

    if($passwordCheck == '')
    {
        $response['code'] = -3;
        $response['message'] = 'Missing or invalid password check (retype password field).';
        return $response;
    }
	
	if($email == '')
	{
		$response['code'] = -4;
		$response['message'] = 'Missing email address.';
		return $response;
	}
	
	// Check to make sure that email provided is valid.
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		$response['code'] = -5;
		$response['message'] = 'Invalid email address.';
		return $response;
	}

    // If the password and password check don't match
    // send back an error.
    if($password != $passwordCheck)
    {
        $response['code'] = 2;
        $response['message'] = 'Passwords don\'t match.';
        return $response;
    }

    // If the password is too short.
    // PASSWORD VALIDATION
    if(strlen($password) < 6)
    {
        $response['code'] = 3;
        $response['message'] = 'Password must be at least 6 characters long.';
        return $response;
    }

    if($users->UserExists($username))
    {
        $response['code'] = 4;
        $response['message'] = 'A user with that username already exists.';
        return $response;
    }

	// Attempt to create the user
	$userId = $users->CreateUser($username, $password, $email);
	
	// If the user ID we get back is 0, then something went wrong.
	if($userId == 0)
	{
		$response['code'] = 1;
		$response['message'] = 'Failed to create user.';
		return $response;
	}

	// If the user ID we get back is not 0, then the user was created successfully
	// and we can send back a response.
	$response['code'] = 0;
	$response['message'] = 'User created.';
	$response['UserID'] = $userId;

    // Log the user in before we hand back the response.
    $_SESSION['userData'] = $users->GetUser($userId);

	return $response;
}

?>