<?php 

require_once('../data/UserRepository.class.php');

function UserGet($userId = -1)
{
	$userRepo = new UserRepository();
	
	$response = array(
		'code' => -1,
		'message' => ''
	);

	if($userId == -1)
	{
		$response['message'] = 'A user ID must be provided.';
		return $response;
	}
	
	$user = $userRepo->GetUser($userId);
    unset($user['Password']);
	
	if($user == null)
	{
		$response['code'] = 1;
		$response['message'] = 'User with ID ' . $userId  . ' does not exist.';
		return $response;
		
	}
	
	$response['code'] = 0;
	$response['message'] = '';
	$response['User'] = $user;
	return $response;
}

?>