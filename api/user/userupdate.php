<?php

require_once('../data/UserRepository.class.php');
require_once('../lib/HashUtil.class.php');

session_start();

function UserUpdate($userId = '', $userData = array())
{
    $userRepo = new UserRepository();
    $hashUtil = new HashUtil('user');

    $response = array(
        'code' => -1,
        'message' => ''
    );

    $decUserId = $hashUtil->Decode($userId, true);

    $response['data'] = $userData;
    if($userData['ID'] == '')
    {
        $response['message'] = 'A user ID must be provided.';
        return $response;
    }

    if(!filter_var($userData['Email'], FILTER_VALIDATE_EMAIL))
    {
        $response['code'] = -2;
        $response['message'] = 'Invalid email.';
        return $response;
    }

    return $response;
}

?>