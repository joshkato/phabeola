<?php

require_once('site.inc.php');
session_start();

function RenderAdminPage()
{
?>
<div class="container">
    <div class="page-header">
        <h1>Administration</h1>
    </div>
    <div class="panel panel-default">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#adminTools" data-toggle="tab">Tools</a></li>
            <li><a href="#adminUsers" data-toggle="tab">Users</a></li>
            <li><a href="#adminImages" data-toggle="tab">Images</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="adminTools">
                <div class="panel-heading">
                    <h4>Tools</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Encrypt / Decrypt</label>
                            <div class="form-group">
                                <div class="input-group">
                                <span class="input-group-btn">
                                    <button id="adminToolBtnEncrypt" class="btn btn-default" type="button" onclick="EncryptValue('#adminToolTxtEncrypt');">Encrypt</button>
                                </span>
                                    <input id="adminToolTxtEncrypt" type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                <span class="input-group-btn">
                                    <button id="adminToolBtnDecrypt" class="btn btn-default" type="button" onclick="DecryptValue('#adminToolTxtDecrypt');">Decrypt</button>
                                </span>
                                    <input id="adminToolTxtDecrypt" type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="adminToolSelCryptMode">Mode</label>
                                <select id="adminToolSelCryptMode" class="form-control form-inline">
                                    <option value="user">User</option>
                                    <option value="image">Image</option>
                                    <option value="album">Album</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="adminUsers">
                <div class="panel-heading">
                    <h4>User Administration</h4>
                </div>
                <div class="panel-body">

                </div>
            </div>
            <div class="tab-pane" id="adminImages">
                <div class="panel-heading">
                    <h4>Image Administration</h4>
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
?>