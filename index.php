<?php

require_once('site.inc.php');
include('index.home.inc.php');
include('index.user.inc.php');
include('index.admin.inc.php');

session_start();

$isLoggedIn = isset($_SESSION['userData']);
$isAdmin = isset($_SESSION['userData']) && $_SESSION['userData']['IsAdmin'];
$userName = '';

if($isLoggedIn)
    $userName = $_SESSION['userData']['LoginName'];

function LoadPage()
{
    global $isLoggedIn;
    global $isAdmin;

    if(!isset($_GET['page']) || $_GET['page'] == '' || strtolower($_GET['page']) == 'home')
    {
        RenderPageHome();
    }
    else if(strtolower($_GET['page']) == 'user')
    {
        RenderUserPage($_GET['arg'], $isLoggedIn);
    }
    else if(strtolower($_GET['page']) == 'album')
    {
        include 'index.album.inc.php';
    }
    else if(strtolower($_GET['page']) == 'image')
    {
        include 'index.image.inc.php';
    }
    else if(strtolower($_GET['page']) == 'admin' && $isAdmin)
    {
        RenderAdminPage();
    }
}

function LoadPageJavascript()
{
    global $isLoggedIn;

    if(!isset($_GET['page']) || $_GET['page'] == '' || strtolower($_GET['page']) == 'home')
    {
        return '';
    }
    else if(strtolower($_GET['page']) == 'user')
    {
        return '<script type="text/javascript" src="js/site.user.js"></script>';
    }
    else if(strtolower($_GET['page']) == 'album')
    {

    }
    else if(strtolower($_GET['page']) == 'image')
    {

    }
    else if(strtolower($_GET['page']) == 'admin')
    {
        return '<script type="text/javascript" src="js/site.admin.js"></script>';
    }

    return '';
}

?>
<!DOCTYPE xhtml>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Phabeola</title>

    	<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet" />
    	
    	<!-- Load the site configuration -->
    	<script type="text/javascript" src="js/config.js"></script>

	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	<link href="css/site.css" rel="stylesheet" />

    </head>

    <body>
    	<!-- modalDialogs -->
        <?php // Only generate the upload modal if the user is logged in.
        if($isLoggedIn) { ?>
    	<!-- upload -->
    	<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    					<h4 class="modal-title" id="myModalLabel">Upload Image</h4>
					</div>
					<div class="modal-body">
						<input id="fileUploadImage" type="file" class="form-control" accept="image/*" onchange="updateImagePreview(this);"/>
						<br />
						<div style="text-align: center;">
							<img id="imgPreview" />
						</div>
						<div class="form-group">
							<label for="uploadImageTitle">Title</label>
							<input type="text" class="form-control" id="uploadImageTitle" placeholder="Title" />
						</div>
						<div class="form-group">
							<label for="uploadImageTitle">Description</label>
							<textarea class="form-control" style="resize: vertical;" rows="8" id="uploadImageDesc"></textarea>
						</div>
						<div class="form-group">
							<label for="uploadCmbAlbums">Album</label>
							<select class="form-control" id="uploadCmbAlbums"></select>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" id="uploadImagePrivate" />
								Private
							</label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="uploadBtnCancel">Close</button>
						<button type="button" class="btn btn-primary" id="uploadBtnUpload">Upload</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.upload -->
        <?php } ?>
		
		<!-- login -->
		<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    					<h4 class="modal-title" id="myModalLabel">Login</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="loginTxtUsername">Username</label>
							<input type="text" class="form-control" id="loginTxtUsername" placeholder="Username">
						</div>
						<div class="form-group">
							<label for="loginTxtPassword">Password</label>
							<input type="password" class="form-control" id="loginTxtPassword" placeholder="Password">
						</div>
						<div id="loginMsg">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="loginBtnCancel">Cancel</button>
						<button type="button" class="btn btn-primary" id="loginBtnLogin">Login</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.login -->
		
		<!-- register -->
		<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
    			<div class="modal-content">
    				<div class="modal-header">
    					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    					<h4 class="modal-title" id="myModalLabel">Register</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="regTxtUserName">Username</label>
							<input type="text" class="form-control" id="regTxtUserName" placeholder="Username">
						</div>
						<div class="form-group">
							<label for="regTxtPassword">Password</label>
							<input type="password" class="form-control" id="regTxtPassword" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="regTxtPasswordCheck">Retype Password</label>
							<input id="regTxtPasswordCheck" type="password" class="form-control" placeholder="Retype Password">
						</div>
						<div class="form-group">
							<label for="regTxtEmail">Email</label>
							<input type="text" class="form-control" id="regTxtEmail" placeholder="Email Address">
						</div>
						<div id="regMsg" style="visibility: hidden;">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="regBtnCancel">Close</button>
						<button type="button" class="btn btn-primary" id="regBtnRegister">Register</button>
					</div>
				</div>
			</div>
		</div>
        <!-- /.register -->

        <!-- info -->
        <div class="modal fade" id="modalDialogInfo" tabindex="-1" role="dialog" aria-labelledby="dialogInfoTitle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="dialogInfoTitle">Information</h4>
                    </div>
                    <div class="modal-body">
                        <div id="dialogInfoText">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="dlgInfoBtnOK">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.info -->
    	<!-- /.modalDialogs -->


    	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo $PHB_ROOT_PATH; ?>">Phabeola</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo $PHB_ROOT_PATH; ?>">Home</a></li>
                            <?php if(isset($_SESSION['userData'])) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Albums <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Add</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Delete</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Images</a></li>
                            <?php } ?>
                            <!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                            -->
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if($isLoggedIn) { ?>
                            <li><a href="#" data-toggle="modal" data-target="#modalUpload"><span class="glyphicon glyphicon-arrow-up" style="color: #0f0;"></span>&nbsp;Upload</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $userName; ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a id="umLnkProfile" href="/phabeola/user/profile">Profile</a></li>
                                    <li><a id="umLnkSettings" href="/phabeola/user/settings">Settings</a></li>
                                    <li class="divider"></li>
                                    <?php if($isAdmin) { ?>
                                    <li><a id="umLnkAdmin" href="/phabeola/admin">Admin</a></li>
                                    <li class="divider"></li>
                                    <?php } ?>
                                    <li><a id="umLnkLogout" href="#">Logout</a></li>
                                </ul>
                            </li>
                            <?php } else { ?>
                            <li><a href="#" data-toggle="modal" data-target="#modalLogin">Login</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalRegister">Register</a></li>
                            <?php } ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
			</div><!-- /.container-fluid -->
		</nav>

        <!-- Page Render -->
        <?php
        LoadPage();
        ?>
        <!-- End Page Render -->

        <footer>
            <?php if($PHB_DEBUG) { ?>
            <div class="container" style="font-family: monospace; font-size: 11px;">
                <p>Requested URL: <?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?></p>
                <p>Page: {<?php echo $_GET['page']; ?>} Arg: {<?php echo $_GET['arg']; ?>}</p>
            </div>
            <?php } ?>
        </footer>

    	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="js/jquery-2.1.0.min.js"></script>
    	<!-- Include all compiled plugins (below), or include individual files as needed -->
    	<script src="js/bootstrap.min.js"></script>
    	<script src="js/site.js"></script>
        <?php if($isLoggedIn) { ?>
    	<script src="js/site.imageupload.js"></script>
        <?php }
        echo LoadPageJavascript();
        ?>
  </body>
</html>
