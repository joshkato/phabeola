<?php

//echo '<script type="text/javascript">location.href = ' . $PHB_ROOT_PATH . '</script>';

require_once('site.inc.php');
require_once('api/lib/HashUtil.class.php');

session_start();

function RenderUserPage($page, $isLoggedIn)
{
    global $PHB_ROOT_PATH;

    if($page == 'profile')
    {
        if($isLoggedIn)
        {
            RenderUserPageProfile();
        }
        else
        {
        ?>
            <script type="text/javascript">location.href="<?php echo $PHB_ROOT_PATH; ?>";</script>
        <?php
        }
        return;
    }

    if($page == 'settings')
    {
        if($isLoggedIn)
        {
            RenderUserPageSettings();
        }
        else
        {
        ?>
            <script type="text/javascript">location.href="<?php echo $PHB_ROOT_PATH; ?>";</script>
        <?php
        }
        return;
    }

    // If we're not loading the profile or settings
    // then we have a user ID and can load the viewable
    // profile for that user.
    RenderUserPageView($page);
    return;
}

function RenderUserPageProfile()
{
    $userData = $_SESSION['userData'];
// +------------------+
// |  Profile Page    |
// +------------------+
?>
<!-- User Profile Modals -->
<div>
    <div class="modal fade" id="profileDlgChangePass" tabindex="-1" role="dialog" aria-labelledby="profileLblChangePass" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="profileLblChangePass">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Password</label>
                        <input id="profileTxtOldPass" type="password" class="form-control" placeholder="Old Password" />
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input id="profileTxtNewPass" type="password" class="form-control" placeholder="New Password" />
                    </div>
                    <div class="form-group">
                        <label>Retype New Password</label>
                        <input id="profileTxtNewPassCheck" type="password" class="form-control" placeholder="Retype New Password" />
                    </div>
                    <div id="profileDlgChangePassMsg">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="ProfileUpdatePassword();">Change</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="profileDlgUpdate" tabindex="-1" role="dialog" aria-labelledby="profileDlgUpdateTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="profileDlgUpdateTitle">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>
                            Enter your current password to update your profile.
                        </p>
                        <label>Password</label>
                        <input id="profileTxtCurrentPass" type="password" class="form-control" placeholder="Password" />
                    </div>
                    <div id="profileDlgUpdateMsg">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End User Profile Modals -->

<div class="container">
    <div class="page-header">
        <h1>Profile</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Information</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="profileTxtUsername">Username</label>
                        <input id="profileTxtUsername" type="text" class="form-control" readonly="readonly" value="<?php echo $userData['LoginName']; ?>" placeholder="Username" />
                    </div>
                    <div class="form-group">
                        <label for="profileTxtEmail">Email</label>
                        <input id="profileTxtEmail" type="text"class="form-control" value="<?php echo $userData['Email']; ?>" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <?php // Just so it matches the rest profile options ?>
                        <label for="profileTxtPassword">Password</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button id="profileBtnChangePass" class="btn btn-default" type="button"  data-toggle="modal" data-target="#profileDlgChangePass">Change</button>
                            </span>
                            <input id="profileTxtPassword" type="password" class="form-control" readonly="readonly" value="************" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="profileTxtEmail">First Name</label>
                        <input id="profileTxtFirstName" type="text" class="form-control" value="<?php echo $userData['FirstName']; ?>" placeholder="First Name" />
                    </div>
                    <div class="form-group">
                        <label for="profileTxtEmail">Last Name</label>
                        <input id="profileTxtLastName" type="text" class="form-control" value="<?php echo $userData['LastName']; ?>" placeholder="Last Name" />
                    </div>
                    <div class="form-group">
                        <label>Birthday</label>
                        <div class="control-group">
                            <div class="controls form-inline">
                                <select id="profileOptDobMonth" class="form-control" onchange="PopulateMonthDays()" ></select>
                                <select id="profileOptDobDay" class="form-control"></select>
                                <select id="profileOptDobYear" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h4>Settings</h4>
            <div class="checkbox">
                <label>
                    Show Email
                    <input type="checkbox" />
                </label>
            </div>
            <div class="checkbox">
                <label>
                    Show First Name
                    <input type="checkbox" />
                </label>
            </div>
            <div class="checkbox">
                <label>
                    Show Last Name
                    <input type="checkbox" />
                </label>
            </div>
            <div id=profileFooterButtons">
                <button id="profileBtnUpdate" class="btn btn-primary" onclick="ViewProfile('<?php echo $userData['ID']; ?>')">View</button>
                <button id="profileBtnViewProfile" class="btn btn-success" data-toggle="modal" data-target="#profileDlgUpdate"><span class="glyphicon glyphicon-ok"></span> Update</button>
                <button id="profileBtnCancel" class="btn btn-default"><span class="text-danger glyphicon glyphicon-remove"></span> Discard Changes</button>
            </div>
        </div>
    </div>
</div>
<?php
// +------------------+
// | End Profile Page |
// +------------------+
}

function RenderUserPageSettings()
{
// +------------------+
// | Settings Page    |
// +------------------+
?>
<div class="container">
    <div class="page-header">
        <h1>Settings</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Image Settings</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="settingsAllowNsfw" />
                        Show NSFW Images
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
// +-------------------+
// | End Settings Page |
// +-------------------+
}

function RenderUserPageView($userId)
{
    $hashUtil = new HashUtil('user');
    $realUserId = $hashUtil->Decode($userId);

    // Check to make sure the ID we're decoding
    // was actually able to be decoded
    if($realUserId == '' || $realUserId == null)
        return; // redirect to 404
?>
<div class="container">
    <div class="page-header">
        <h1>View Profile</h1>
    </div>
    <p>Profile for user #<?php echo $realUserId; ?></p>
</div>
<?php
}
?>
