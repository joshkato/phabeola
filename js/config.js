function GetConfig(option)
{
	var configOptionValue;
		
	$.ajax({
		async: false, // Needed to get the value to return.
		type: "POST",
		url: "api/config",
		data: {
			option: option
		},
		dataType: "json",
		success: function(response) {
			configOptionValue = response.value;	
		}
	});
	
	if(configOptionValue == undefined)
	{
		throw new Error('Failed to retreive configuration option.');	
	}
	
	return configOptionValue;
}