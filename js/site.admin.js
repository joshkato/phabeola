$(function(){

});

function EncryptValue(encTxtBox)
{
    $('#adminToolTxtDecrypt').val('');
    var valueToEncrypt = $(encTxtBox).val();
    if(isNaN(valueToEncrypt) || valueToEncrypt == '')
    {
        ShowInfoDialog('Invalid Value', '<div class="alert alert-danger">Only numerical values can be encrypted.</div>');
        $('#adminToolTxtDecrypt').val();
        return;
    }

    var encryptedValue = CryptApi($('#adminToolSelCryptMode').val(), valueToEncrypt, true);

    if(encryptedValue === '')
    {
        ShowInfoDialog('Encrypt Failed', '<div class="alert alert-danger">Failed to encrypt the value ' + valueToEncrypt + '.</div>');
        return;
    }

    $('#adminToolTxtDecrypt').val(encryptedValue);
}

function DecryptValue(decTxtBox)
{
    $('#adminToolTxtEncrypt').val('');
    var valueToDecrypt = $(decTxtBox).val();

    var decryptedValue = CryptApi($('#adminToolSelCryptMode').val(), valueToDecrypt, false);

    if(decryptedValue === '' || decryptedValue == null)
    {
        ShowInfoDialog('Decrypt Failed', '<div class="alert alert-danger">Failed to decrypt the value ' + valueToDecrypt + '. Make sure you are you using the correct mode.</div>');
        return;
    }

    $('#adminToolTxtEncrypt').val(decryptedValue);
}

function CryptApi(type, value, encrypt)
{
    var cryptValue = '';

    if(isNaN(value) && encrypt)
    {
        return cryptValue;
    }

    $.ajax({
        type: "POST",
        url: "/phabeola/api/hash.php",
        data: {
            mode: encrypt ? "encrypt" : "decrypt",
            type: type,
            value: value
        },
        dataType: "json",
        async: false,
        success: function(response)
        {
            if(response.code == 0)
            {
                cryptValue = response.value;
            }
        }
    });

    return cryptValue;
}