$(function(){
	
});

function updateImagePreview(input)
{
	if(input.files && input.files[0])
	{
		var reader = new FileReader();
		reader.onload = function(e){
			$('#imgPreview').attr('src', e.target.result).width(230);
		};
		
		reader.readAsDataURL(input.files[0]);
	}
}

function getImageData(input)
{
	if(input.files && input.files[0])
	{
		var reader = new FileReader();
		return reader.readAsBinaryString();
	}

	return 0;
}

function getImageObject()
{
	var imageObj = {
		
	};
}
