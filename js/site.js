$(function() {

    // Login Modal Buttons
	$('#loginBtnLogin').click(function(){
		UserLogin($('#loginTxtUsername').val(), $('#loginTxtPassword').val());
	});

    $('#loginBtnCancel').click(function(){
       // Do something.
    });

    $('#loginTxtUsername').keypress(function(e){
        if(e.keyCode == 13){
            UserLogin($('#loginTxtUsername').val(), $('#loginTxtPassword').val());
        }
    });

    $('#loginTxtPassword').keypress(function(e){
        if(e.keyCode == 13){
            UserLogin($('#loginTxtUsername').val(), $('#loginTxtPassword').val());
        }
    });
	// End Login Modal Buttons

    // Register Modal Buttons
    $('#regBtnRegister').click(function(){
        UserRegister();
    })

    $('#regBtnCancel').click({
        // Do something
    });

    // End Register Modal Buttons

    // User Menu Buttons

    $('#umLnkProfile').click(function() {

    });

	$('#umLnkLogout').click(function(){
		UserLogout();
	});

    // End User Menu Buttons.
});

function UserLogin(userName, password)
{
	var loginUrl = GetConfig('loginurl');
	
	$.ajax({
        async: false, // Don't do the login async.
		type: "POST",
		url: loginUrl,
		data: {
			username: userName,
			password: password
		},
		dataType: "json",
		success: function(response) {
			if(response['code'] == 0) {
				$('#loginMsg').css('visibility', 'hidden');
				location.reload();
			}
			else {
				$('#loginMsg')
                    .css('visibility', 'visible')
                    .html('<div class="alert alert-danger">Login failed. Please check your username and password.</div>');
			}
		}
	});
}

function UserLogout()
{
	var logoutUrl = GetConfig('logouturl');
	
	$.ajax({
        async: false, // Don't do the logout async.
		type: "GET",
		url: logoutUrl,
		data: {},
		dataType: "json"
	});
	
	location.reload();
}

function UserRegister()
{
	var registerUrl = GetConfig('registerurl');

    var userName = $('#regTxtUserName');
    var password = $('#regTxtPassword');
    var passwordCheck = $('#regTxtPasswordCheck');
    var email = $('#regTxtEmail');

    if(!ValidateRegisterInfo())
        return;
	
	$.ajax({
		type: "POST",
		url: registerUrl,
		data: {
            username: userName.val(),
            password: password.val(),
            passwordCheck: passwordCheck.val(),
            email: email.val()
		},
		dataType: "json",
		success: function(response) {
            if(response.code != 0)
            {
                // Display the error message area
                $('#regMsg')
                    .css('visibility', 'visible')
                    .removeClass()
                    .addClass('alert')
                    .addClass('alert-danger')
                    .html(response.message);

                // Reset the formatting on all of the
                // input textboxes.
                SetElementOk(userName);
                SetElementOk(password);
                SetElementOk(passwordCheck);
                SetElementOk(email);

                switch(response.code)
                {
                    case -1:
                        SetElementErrored(userName);
                        break;
                    case -2:
                        SetElementErrored(password);
                        break;
                    case -3:
                        SetElementErrored(passwordCheck);
                        break;
                    case -4:
                    case -5:
                        SetElementErrored(email);
                        break;
                    case 2:
                        SetElementErrored(password);
                        SetElementErrored(passwordCheck);
                        break;
                    case 3:
                        SetElementErrored(password);
                        break;
                    case 4:
                        SetElementErrored(userName)
                        break;
                    default:
                        break;
                }

            }
            else
            {
                location.reload();
            }
		}
	});
}

function ValidateRegisterInfo()
{
    var validationStatus = new Array();
    var validationMessages = new Array();

    var userName = $('#regTxtUserName');
    var password = $('#regTxtPassword');
    var passwordCheck = $('#regTxtPasswordCheck');
    var email = $('#regTxtEmail');

    // Validate User Name
    if(userName.val() == '') {
        SetElementErrored(userName);

        validationStatus.push(false);
        validationMessages.push('A username must be provided.');
    }
    else {
        SetElementOk(userName);
        validationStatus.push(true);
    }

    // Validate Password
    if(password.val() == '') {
        console.log('halp');
        SetElementErrored(password);

        validationStatus.push(false);
        validationMessages.push('A password is required.');
    }
    else {
        SetElementOk(password);
        validationStatus.push(true);
    }

    // Validate Password Check
    if((password.val() != passwordCheck.val())){
        SetElementErrored(passwordCheck);

        validationStatus.push(false);
        validationMessages.push('Passwords don\'t match.');
    }
    else if(password.val() == '') {
        SetElementErrored(passwordCheck);
    }
    else {
        SetElementOk(passwordCheck);
        validationStatus.push(true);
    }

    // Validate Email
    if(email.val() == '') {
        SetElementErrored(email);

        validationStatus.push(false);
        validationMessages.push('An email address is required.');
    }
    else if(email.val().indexOf('@') < 0 || email.val().indexOf('.') < 0) {
        SetElementErrored(email);

        validationStatus.push(false);
        validationMessages.push('A valid email address is required.');
    }
    else {
        SetElementOk(email);
        validationStatus.push(true);
    }

    if(validationStatus.indexOf(false) >= 0)
    {
        // Make the message box visible and reset
        // the formatting to be an error message.
        $('#regMsg')
            .css('visibility', 'visible')
            .removeClass()
            .addClass('alert')
            .addClass('alert-danger')
            .html('')
            .append(validationMessages.join('<br/>'));

        return false;
    }
    else
    {
        $('#regMsg')
            .css('visibility', 'hidden')
            .removeClass();

        return true;
    }
}

// Styling functions

function SetElementErrored(element)
{
    element.css('background', '#f2dede');
}

function SetElementOk(element)
{
    element.css('background', '#fff');
}

// End Styling Functions

// General Dialog Functions

function ShowInfoDialog(title, dialogBody, okCallBack)
{
    $('#dialogInfoTitle').html(title);
    $('#dialogInfoText').html(dialogBody);
    $('#dlgInfoBtnOK').click(okCallBack);
    $('#modalDialogInfo').modal('show');
}

// End General Dialog Functions