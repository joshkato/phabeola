$(function() {
    // Populate Date of Birth Dropdowns
    PopulateDobSelections();

    // Profile Events

    $('#profileTxtOldPass').keypress(function(e){
        if(e.keyCode == 13){
            ProfileUpdatePassword();
        }
    });

    $('#profileTxtNewPass').keypress(function(e){
        if(e.keyCode == 13){
            ProfileUpdatePassword();
        }
    })

    $('#profileTxtNewPassCheck').keypress(function(e){
        if(e.keyCode == 13){
            ProfileUpdatePassword();
        }
    });


    // End Profile Events
});

function ViewProfile(userId)
{
    location.href = '/phabeola/user/' + userId;
}

function PopulateDobSelections()
{
    PopulateMonths();
    PopulateMonthDays();
    PopulateYears();
}

function PopulateMonths()
{
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var monthOpts = "";

    for(var i = 0; i < months.length; i++)
    {
        monthOpts += "<option value=\"" + (i + 1) + "\">" + months[i] + "</option>";
    }

    $('#profileOptDobMonth').html(monthOpts);
}

function PopulateYears()
{
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var yearStr = "";

    for(var i = currentYear; i >= currentYear - 100; i--)
    {
        yearStr += "<option value=\"" + i + "\">" + i + "</option>";
    }

    $('#profileOptDobYear').html(yearStr);
}

function PopulateMonthDays()
{
    $('#profileOptDobDay').html('');

    var dayLimit = 30;
    var dayStr = "";
    var month = $('#profileOptDobMonth').val();

    if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
    {
        dayLimit = 31;
    }

    if(month == 2)
    {
        dayLimit = 29;
    }

    for(var i = 1; i <= dayLimit; i++)
    {
        dayStr += "<option value=\"" + i + "\">" + i + "</option>";
    }

    $('#profileOptDobDay').html(dayStr);
}

function ProfileUpdatePassword()
{
    var oldPass = $('#profileTxtOldPass').val();
    var newPass = $('#profileTxtNewPass').val();
    var newPassCheck = $('#profileTxtNewPassCheck').val();
    var messageArea = $('#profileDlgChangePassMsg');
    var pwUpdateUrl = GetConfig('pwupdateurl');

    $.ajax({
        type: "POST",
        url: pwUpdateUrl,
        data: {
            oldpass: oldPass,
            newpass: newPass,
            newpasscheck: newPassCheck
        },
        dataType: "json",
        success: function(response) {

            if(response.code == 0)
            {
                messageArea.html('<div class="alert alert-success">Password updated.</div>');
                window.setTimeout(function() {
                    $('#profileDlgChangePass').modal('hide');
                }, 2000)
            }
            else
            {
                messageArea.html('<div class="alert alert-danger">Failed to update password.</div>');
            }
        },
        error: function(response) {
            messageArea.html('<div class="alert alert-danger">Failed to update password.</div>');
        }
    });
}
