<?php

class Utility
{
    public static function GetYearList()
    {
        $yearList = array();

        for($i = (date('Y') - 13); $i >= 1900; $i++)
        {
            array_push($yearList, '<option value="' . $i .'">' . $i . '</option>');
        }

        return $yearList;
    }
}

?>